<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Recetas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('medicamento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('descripcion');
            $table->timestamps();
        });
        
        Schema::create('receta', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->string('tratamiento');
            $table->string('recomendaciones');
            $table->integer('medicamento_id')->unsigned();
            $table->foreign('medicamento_id')->references('id')->on('medicamento');
            $table->integer('atencion_id')->unsigned();
            $table->foreign('atencion_id')->references('id')->on('atencion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('receta');
    }
}
