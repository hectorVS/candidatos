<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->increments('id');
            $table->char('dni',8)->unique();
            $table->string('nombres',35);
            $table->string('apPaterno',35);
            $table->string('apMaterno',35);
            $table->char('sexo',1);
            $table->date('fechaNacimiento');
            $table->string('direccion',60);
            $table->string('telefono',15);
            $table->boolean('estado')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cliente');
    }
}
