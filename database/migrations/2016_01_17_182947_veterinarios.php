<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Veterinarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('veterinario', function (Blueprint $table) {
            $table->increments('id');
            $table->char('dni',8)->unique();
            $table->string('nombres',35);
            $table->string('apPaterno',35);
            $table->string('apMaterno',35);
            $table->char('sexo',1);
            $table->date('fechaNacimiento');
            $table->string('direccion',60);
            $table->boolean('estado')->default(true);
            $table->string('clave',60);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('veterinario');
    }
}
