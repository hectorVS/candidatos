<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Atenciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atencion', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('fechaCita');
            $table->integer('numConsultorio');
            $table->string('diagnostico');
            $table->string('enfermedad');
            $table->string('observacion');
            $table->enum('estado',['cita','atencion'])->default('cita');
            $table->float('precioTotal');
            $table->integer('mascota_id')->unsigned();
            $table->integer('veterinario_id')->unsigned();
            $table->integer('tipoatencion_id')->unsigned();
            $table->foreign('mascota_id')->references('id')->on('mascota');
            $table->foreign('veterinario_id')->references('id')->on('veterinario');
            $table->foreign('tipoatencion_id')->references('id')->on('tipoatencion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('atencion');
    }
}
