<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Vacunaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacunacion', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->integer('vacuna_id')->unsigned();
            $table->integer('atencion_id')->unsigned();
            $table->foreign('vacuna_id')->references('id')->on('vacuna');
            $table->foreign('atencion_id')->references('id')->on('atencion');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vacunacion');
    }
}
