<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mascotas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mascota', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',35);
            $table->char('sexo',1);
            $table->integer('edad');
            $table->float('tamanio');
            $table->string('color',15);
            $table->text('descripcion');
            $table->integer('cliente_id')->unsigned();
            $table->integer('especie_id')->unsigned();

            $table->foreign('cliente_id')->references('id')->on('cliente');
            $table->foreign('especie_id')->references('id')->on('especie');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mascota');
    }
}
