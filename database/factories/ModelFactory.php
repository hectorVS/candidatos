<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => str_random(10),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Cliente::class, function ($faker) {
    return [
    	'dni' => str_random(8),
        'nombres' => $faker->name,
        'apPaterno' => $faker->lastName,
        'apMaterno' => $faker->lastName,
        'sexo' =>  'M',
        'fechaNacimiento' => $faker->date,
        'direccion'=> $faker->address,
        'telefono'=> str_random(10)
    ];
});


$factory->define(App\TipoAtencion::class, function ($faker) {
    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->creditCardType,
        'precio' => $faker->randomFloat($nbMaxDecimals = 3, $min = 1, $max = 100)
    ];
});


$factory->define(App\Veterinario::class, function ($faker) {
    return [
        'dni' => rand(10,999),
        'nombres' => $faker->name,
        'apPaterno' => $faker->lastName,
        'apMaterno' => $faker->lastName,
        'sexo' =>  'M',
        'fechaNacimiento' => $faker->date,
        'direccion'=> $faker->address,
        'clave'=> bcrypt('123456')
    ];
});



$factory->define(App\Especie::class, function ($faker) {
    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->LastName,
        ];
});