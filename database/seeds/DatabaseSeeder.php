<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*$data =["dni"=>"7143272",
                "nombres"=>"carlos christian",
                "apPaterno"=>"benavides",
                "apMaterno"=>"montenegro",
                "sexo"=>"M",
                "fechaNacimiento"=>'1991-05-19',
                "direccion"=>"pazta urb quiñonez",
                "telefono"=>"947466999"
                ];
        \App\Cliente::create($data); */   
        Model::unguard();

        /*$this->call('UserTableSeeder');*/
        /*factory('App\Cliente',30)->create();
        factory('App\TipoAtencion',10)->create();*/
       /* factory('App\Veterinario',10)->create();*/
     /*  factory('App\Especie',5)->create();
*/
        Model::reguard();
     /*   $mascota_1 =["nombre"=>"bob",
                    "sexo"=>"M",
                    "edad"=>3,
                    "tamanio"=>"1m",
                    "color"=>"rojo",
                    "descripcion"=>"es rojo",
                    "cliente_id"=>2,
                    "especie_id"=>2 ];*/

       /* $mascota_2 =["nombre"=>"tintiko",
                    "sexo"=>"M",
                    "edad"=>2,
                    "tamanio"=>"2m",
                    "color"=>"azul",
                    "descripcion"=>"es azul",
                    "cliente_id"=>2,
                    "especie_id"=>1 ];*/

      /*  \App\Mascota::create($mascota_1);*/
      /*  \App\Mascota::create($mascota_2);*/

      $atencion_1 = ["fechaCita"=>time(),
                    "numConsultorio"=>rand(1,10),
                    "diagnostico"=>"diarrea",
                    "enfermedad"=>"malss",
                    "observacion"=>"verlo todos",
                    "precioTotal"=>23.3,
                    "mascota_id"=>3,
                    "veterinario_id"=>3,
                    "tipoatencion_id"=>5    
                    ];
     $atencion_2 = ["fechaCita"=>time(),
                    "numConsultorio"=>rand(1,10),
                    "diagnostico"=>"calama",
                    "enfermedad"=>"calambre",
                    "observacion"=>"verlo todssdsos",
                    "precioTotal"=>100.3,
                    "mascota_id"=>3,
                    "veterinario_id"=>4,
                    "tipoatencion_id"=>1
                    ];
            \App\Atencion::create($atencion_1);
            \App\Atencion::create($atencion_2);
    }
}
