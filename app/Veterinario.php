<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Veterinario extends Model
{
    protected $table = "veterinario";
    protected $fillable = ['dni',
    						'nombres',
    						'apPaterno',
    						'apMaterno',
    						'sexo',
    						'fechaNacimiento',
    						'direccion',
    						'clave'];

    protected function atenciones(){
    	return $this->hasMany('App\Atencion');
    }

    protected function telefonos(){
    	return $this->hasMany('App\Telefono');
    }
}
