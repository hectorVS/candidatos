<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mascota extends Model
{
    protected $table = "mascota";
    protected $fillable = ['nombre',
	    					'sexo',
	    					'edad',
	    					'tamanio',
	    					'color',
	    					'descripcion',
	    					'cliente_id',
	    					'especie_id'];
	public function cliente(){
		return $this->belongsTo('App\Cliente');
	}

	public function especie(){
		return $this->belongsTo('App\Especie');
	}

	public function atenciones(){
		return $this->hasMany('App\Atencion');
	}
}
