<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacuna extends Model
{
    protected $table = "vacuna";
    protected $fillable = ['nombre',
    						'precio'];

   	public function atenciones(){
   		return $this->belongsToMany('App\Atencion');
   	}
}
