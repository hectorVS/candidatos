<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoAtencion extends Model
{
    protected $table = "tipoatencion";
    protected $fillable = ['nombre',
    						'descripcion',
    						'precio'];

    public function atenciones(){
    	return $this->hasMany('App\Atencion');
    }
}
