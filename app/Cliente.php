<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = "cliente";
    protected $fillable = ['dni', 
    					'nombres',
    					'apPaterno',
    					'apMaterno',
    					'sexo',
    					'fechaNacimiento',
    					'direccion',
    					'telefono'];
   	public function mascotas(){
   		return $this->hasMany('App\Mascota');
   	}
}
