<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Especie extends Model
{
    protected $table = "especie";
    protected $fillable = ['nombre',
    					'descripcion'];

   	public function mascotas(){
   		return $this->hasMany('App\Mascota');
   	}
}
