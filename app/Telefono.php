<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telefono extends Model
{
    protected $table = "telefono";
    protected $fillable = ['numTelefono',
    					'veterinario_id'];

   	protected function veterinario(){
   		return $this->belongsTo('App\Veterinario');
   	}
   	
}
