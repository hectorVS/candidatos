<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicamento extends Model
{
    protected $table = "medicamento";
    protected $fillable = ['nombre','descripcion'];

    public function atenciones(){
    	return $this->belongsToMany('App\Atencion');
    }
}
