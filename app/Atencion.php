<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atencion extends Model
{
    protected $table = "atencion";
    protected $fillable = ['fechaCita',
    					'numConsultorio',
    					'diagnostico',
    					'enfermedad',
    					'observacion',
    					'precioTotal',
    					'mascota_id',
    					'veterinario_id',
    					'tipoatencion_id'];

    public function mascota(){
    	return $this->belongsTo('App\Mascota');
    }

    public function veterinario(){
    	return $this->belongsTo('App\Veterinario');
    }
    public function medicamentos(){
        return $this->belongsToMany('App\Medicamento')
                     ->withPivot('tratamiento', 'recomendaciones','fecha');
    }       
    public function vacunas(){
        return $this->belongsToMany('App\Vacuna')
                     ->withTimestamps();
    }        
    public function tipoAtencion(){
        return $this->belongsTo('App\TipoAtencion');
    }         
}
